$(document).ready(function() {

  // Fetch JSON file into info
	$.getJSON('data/images.json', function(info){

      var output = '';
      for (var i=0; i< info.length; i++){
        output += '<div class="col-md-4"><li class=""><img class="img-thumbnail" src="' + info[i].media.images.standard_resolution.url + '"><div class="caption">'+
        info[i].caption +'<div>'+ getFormattedDate(Number(info[i].datetime)) +'</div></div></li></div>';
      }

      // Rendering out to the screen
      $('#render').append(output);

	}); //getJSON

// Conver Unix Time Stamp to Date Format: October 21st 2014
  function getFormattedDate(timestamp){

    var fdate = new Date(timestamp * 1000);
    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    var year = fdate.getFullYear();
    var month = months[fdate.getMonth()];
    var fday = getFormattedDay(fdate.getDate());
    var formattedDate = fday + ' ' + month + ' ' + year;
    return formattedDate;
  }

  // Adding Ordinal to the Day
  function getFormattedDay(day){
    switch (day) {
    case 1:
    case 21:
    case 31:
        return day + 'st';
    case 2:
    case 22:
        return day + 'nd';
    case 3:
    case 23:
        return day + 'rd';
    default:
        return day + 'th';
    }
  }

}); // ready
